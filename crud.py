from  tkinter import *
from tkinter import messagebox
import sqlite3

#------------------------------

def conexionBBDD():
	miConexion=sqlite3.connect("usuarios")

	miCurso=miConexion.cursor()
	try:
		miCurso.execute('''
			CREATE TABLE DATOSUSUARIOS(
			ID INTEGER VARCHAR(50),
			NOMBRE_USUARIOS VARCHAR(50),
			PASSWORD VARCHAR(50),
			APELLIDO VARCHAR(20),
			DIRECCION VARCHAR(50),
			COMENTARIO VARCHAR(100))
			''')
		messagebox.showinfo("BBDD ", "BASE DE DATOS Creada con èxito ! ")

	except:
		messagebox.showwarning("¡Ateción ! "," La BBDD ya existe")



def salirAplicacion():
	valor=messagebox.askquestion("Salir","Desea Salir de la Aplicación?")
	if valor=="yes":
		raiz.destroy()

#--------------------------------------------------------

raiz=Tk()


#---------- menu de parte princiapla

barraMenu=Menu(raiz)
raiz.config(menu=barraMenu, width=300, height=300)

baseDatosMenu=Menu(barraMenu,tearoff=0)
baseDatosMenu.add_command(label="Conectar",command=conexionBBDD)
baseDatosMenu.add_command(label="Salir",command=salirAplicacion)

borrarMenu=Menu(barraMenu,tearoff=0)
borrarMenu.add_command(label="Borrar Campo")

crudDatosMenu=Menu(barraMenu,tearoff=0)
crudDatosMenu.add_command(label="Crear")
crudDatosMenu.add_command(label="Leer")
crudDatosMenu.add_command(label="Actulizar")
crudDatosMenu.add_command(label="Borrar")

ayudaMenu=Menu(barraMenu,tearoff=0)
ayudaMenu.add_command(label="Licencia")
ayudaMenu.add_command(label="Acerca")

barraMenu.add_cascade(label="BBDD",menu=baseDatosMenu)
barraMenu.add_cascade(label="Borrar",menu=borrarMenu)
barraMenu.add_cascade(label="CRUD",menu=crudDatosMenu)
barraMenu.add_cascade(label="Ayuda",menu=ayudaMenu)

#------------------- comienzo de campo --------------

miFrame=Frame(raiz)
miFrame.pack()

cuadroID=Entry(miFrame)
cuadroID.grid(row=0,column=1,padx=10,pady=10)

cuadroNombre=Entry(miFrame)
cuadroNombre.grid(row=1,column=1,padx=10,pady=10)
cuadroNombre.config(fg="blue", justify="center")

cuadroPass=Entry(miFrame)
cuadroPass.grid(row=2,column=1,padx=10,pady=10)
cuadroPass.config(fg="orange", justify="center",show="*")

cuadroApellido=Entry(miFrame)
cuadroApellido.grid(row=3,column=1,padx=10,pady=10)

cuadroDirec=Entry(miFrame)
cuadroDirec.grid(row=4,column=1,padx=10,pady=10)

textoComent=Text(miFrame,width=16,height=4)
textoComent.grid(row=5,column=1,padx=10,pady=10)
scrollVert=Scrollbar(miFrame,command=textoComent.yview)
scrollVert.grid(row=5,column=2,sticky="nsew")
textoComent.config(yscrollcommand=scrollVert.set)

#---------------- label -------------------
idLabel=Label(miFrame,text="Id:")
idLabel.grid(row=0,column=0,sticky="e",padx=10,pady=10)

nombreLabel=Label(miFrame,text="Nombre:")
nombreLabel.grid(row=1,column=0,sticky="e",padx=10,pady=10)

passLabel=Label(miFrame,text="Password:")
passLabel.grid(row=2,column=0,sticky="e",padx=10,pady=10)

apellidoLabel=Label(miFrame,text="Apellido:")
apellidoLabel.grid(row=3,column=0,sticky="e",padx=10,pady=10)

dirLabel=Label(miFrame,text="Dirección:")
dirLabel.grid(row=4,column=0,sticky="e",padx=10,pady=10)

comenLabel=Label(miFrame,text="Comentario:")
comenLabel.grid(row=5,column=0,sticky="e",padx=10,pady=10)

#----------------- aqui los botones--------------

miFrameInferior=Frame(raiz)
miFrameInferior.pack()

botonCrear=Button(miFrameInferior,text="Crear")
botonCrear.grid(row=1,column=0,sticky="e",padx=10,pady=10)

botonLeer=Button(miFrameInferior,text="Leer")
botonLeer.grid(row=1,column=1,sticky="e",padx=10,pady=10)

botonActulizar=Button(miFrameInferior,text="Actulizar")
botonActulizar.grid(row=1,column=2,sticky="e",padx=10,pady=10)

botonBorrar=Button(miFrameInferior,text="Borrar")
botonBorrar.grid(row=1,column=3,sticky="e",padx=10,pady=10)



raiz.mainloop()